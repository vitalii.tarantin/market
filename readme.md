<h1>Develop markup market page.</h1>
### The technology I used
<p>
<a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML5" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/html5-colored.svg" width="36" height="36" alt="HTML5" /></a><a href="https://www.w3.org/TR/CSS/#css" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/css3-colored.svg" width="36" height="36" alt="CSS3" /></a><a href="https://www.figma.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/figma-colored.svg" width="36" height="36" alt="Figma" /></a>
</p>
### gitlab pages
https://market-vitalii-tarantin-6df4554690d560ea2a923c917328ebf6ae4e8c6.gitlab.io/<br>
### layout
https://www.figma.com/file/wNzd8LV7TVgH0FcG1z4pHK/%F0%9F%9B%92-Ecommerce-Shopping-Template-(Community)?node-id=0%3A1&mode=dev
